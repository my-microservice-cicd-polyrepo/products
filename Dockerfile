FROM node:latest

WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install
COPY server.js .

EXPOSE 3001
CMD ["npm", "start"]
